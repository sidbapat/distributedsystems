CSCI-652 Distributed Systems
For more info, please visit my [teaching](https://www.cs.rit.edu/~ph/teaching) page.

For RIT students, you can find more information on myCourses.

Run ```mvn package``` inside the folder of each module to compile and generate a .jar file.


If you need to sync your forked repo with upstream repo, check [here](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/syncing-a-fork)

# Sync up to this repo

Add `upstream` repo URL to your forked repo
```bash
git remote add upstream https://gitlab.com/SpiRITlab/distributedsystems.git
```

Then if you type ```git remote -v``` if should show the following
```bash
origin	https://gitlab.com/YOUR-USERNAME/distributedsystems.git (fetch)
origin	https://gitlab.com/YOUR-USERNAME/distributedsystems.git (push)
upstream	https://gitlab.com/SpiRITlab/distributedsystems.git (fetch)
upsream		https://gitlab.com/SpiRITlab/distributedsystems.git (push)
```

Now, run this command to pull from this repo
```bash
git pull upstream master
```

Once, the pull command is done. Your repo should have my latest changes. Then push the changes to your forked repo.
```bash
git push
```

Note, you might need to resolve conflicts if there are mismatches in the two repo.


# Docker container
Build image with
```
docker build -t csci652:latest .
```

Verify the new image with 
```
docker images
```

Start a container
```
docker run -it csci652 /bin/bash
```


# Docker-compose
Build and start docker containers
```
docker-compose up
```
use `--build` to rebuild docker image

Attach to the running docker containers
```


docker exec -it <CONTAINER-NAME> bash
```
where `<CONTAINER-NAME>` should be replaced with your targeted container name.

# Execution instructions:
For this project, I used the Docker Toolbox for Windows.
It includes the use of virtualbox, Docker Quickstart terminal, and Kitematic.
(All of the above are automatically installed with the install package)

Copy the git contents into a folder in your system

I used my own Java codes for this project

Create a folder for storing the java codes and the csv file in the Dockerfile
```
COPY project1 /csci652/project1
```
Copy the java codes and the csv file into that folder

If you need to copy more files to the same folder,
you may have to use below in Dockerfile
```
COPY project1/<filename> /csci652/project1
```

Update the docker-compose file if you wish to change the container names

Start the Docker Quickstart terminal and once it is set up,
go to the appropriate directory where you stored the git files

Run the command to create images
```
docker-compose build
```

Run the command to create separate containers for each "peer"
```
docker-compose up -d
```

Run the command to set up program execution
```
docker exec -it <CONTAINER1-NAME> bash
```
go to the appropriate folder containing your .java files and run the server

Open another Docker Quickstart terminal
Go to the directory containing the git files
Skip the build and up steps and directly run
```
docker exec -it <CONTAINER2-NAME> bash
```
go to the appropriate folder containing your .java files and run the client.
The client requires two main function arguments:
The first is the file, the second is the server container name

The client sends the file contents to the server and the server returns the total word count.
This code works mostly for decent sized files. Large files may pose problems.

