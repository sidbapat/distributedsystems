package pubsub;

/*
 * PubSubAgent.java
 */
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class demonstrates the working of a publisher or subscriber of a pubsub
 * distributed system.
 *
 * @author Siddharth Bapat
 */

public class PubSubAgent implements Publisher, Subscriber{

    //port of pubsubagent
    int port=2500;
    //list of all notifications received
    ArrayList<String> notifications=new ArrayList<>();
    //list of all subscribed topics
    ArrayList<String> subscribedtopics=new ArrayList<>();

    //socket programming variables
    Socket soc;
    InputStream ins;
    OutputStream outs;

    //scanner to read user input
    Scanner userinp = new Scanner(System.in);


    @Override
    public void subscribe(Topic topic) {
        // TODO Auto-generated method stub

    }

    /**
     * This function is used to subscribe to a particular topic
     * @param       keyword     topic name
     * @throws IOException
     */
    @Override
    public void subscribe(String keyword) throws IOException {
        int suback=0;
        outs.write(keyword.getBytes());
        while ((suback=ins.read())>0){
            if((char)suback=='$'){
                break;
            }
            if((char)suback=='Y'){
                System.out.println("If you provided the correct name then you are " +
                        "subscribed otherwise your list is empty");
                break;
            }
        }
    }

    @Override
    public void unsubscribe(Topic topic) {
        // TODO Auto-generated method stub

    }

    /**
     * This function is used to unsubscribe from a particular topic
     * @param       str     unsubscribe request containing topic name
     * @throws IOException
     */
    @Override
    public void unsubscribe(String str) throws IOException {
        int p;
        outs.write(str.getBytes());
        while((p=ins.read())>0){
            if ((char) p == '$') {
                break;
            }
            if ((char) p == 'Y') {
                System.out.println("Unsubscribed from topic");
                break;
            }
        }
    }


    /**
     * This function is used to list all the subscribed topics
     * @throws IOException
     */
    @Override
    public void listSubscribedTopics() throws IOException {
        System.out.println("");
        System.out.println("Subscribed Topics");
        for(int s=0;s<subscribedtopics.size();s++){
            System.out.println(subscribedtopics.get(s));
        }
        System.out.println("");
    }

    /**
     * This function is used to publish an event
     * @param       event       event object
     * @throws IOException
     */
    @Override
    public void publish(Event event) throws IOException {
        outs.write(("pub_"+event.content+"_"+event.topic.name+"$").getBytes());
        System.out.println("Published");

    }

    /**
     * This function is used to advertise a new topic
     * @param newTopic
     * @throws IOException
     */
    @Override
    public void advertise(Topic newTopic) throws IOException {
        outs.write(("ad_"+newTopic.name+"$").getBytes());
        System.out.println("Advertised");

    }

    /**
     * This function is used to enable the user to select actions to perform
     * @param       usr     the username that logged in
     * @throws IOException
     * @throws InterruptedException
     */
    public void operations(String usr) throws IOException, InterruptedException {
        int p;
        String op1;
        lp:while(true) {
            System.out.println("What would you like to do?");
            System.out.println("1. subscribe");
            System.out.println("2. unsubscribe");
            System.out.println("3. publish");
            System.out.println("4. advertise");
            System.out.println("5. skip");
            System.out.println("6. show subscribed topics");
            String op=userinp.nextLine();
            System.out.println("selected option:"+op);
            //
            soc = new Socket("EventManager", 2000);
            ins = soc.getInputStream();
            outs = soc.getOutputStream();
            //
            switch(op){
                case "1": outs.write(("sub_"+usr+"$").getBytes());
                            System.out.println("Please specify the name to subscribe:");

                            while((p=ins.read())>0){
                                if ((char) p == '$') {
                                    break;
                                }
                                System.out.print((char)p);
                            }
                            op1=userinp.nextLine();
                            subscribe(op1+"$");


                    //
                    ins.close();
                    outs.close();
                    soc.close();
                    //
                            break;
                case "2":   System.out.println("Select topic name to unsubscribe");
                            op1=userinp.nextLine();
                            unsubscribe("unsub_"+op1+"_"+usr+"$");
                    //
                    ins.close();
                    outs.close();
                    soc.close();
                    //
                            break;
                case "3":   System.out.println("Publish your event");
                            op1=userinp.nextLine();
                            System.out.println("Specify topic");
                            String op2=userinp.nextLine();
                            Event pube=new Event(0,new Topic(0,null,op2),"Winner",
                                    op1);
                            publish(pube);
                    //
                    ins.close();
                    outs.close();
                    soc.close();
                    //
                            break;
                case "4":   System.out.println("Advertise the topic");
                            op1=userinp.nextLine();
                            Topic adtop=new Topic(0,null,op1);
                            advertise(adtop);
                    //
                    ins.close();
                    outs.close();
                    soc.close();
                    //
                            break;
                case "6":   String tlst="";
                            outs.write(("List_"+usr+"$").getBytes());
                            while((p=ins.read())>0){
                                if((char)p=='$'){
                                    break;
                                }
                                if(!((char)p=='_')){
                                    tlst+=String.valueOf((char)p);
                                }
                                if((char)p=='_'){
                                    subscribedtopics.add(tlst);
                                    tlst="";
                                }
                            }
                            listSubscribedTopics();
                    //
                    ins.close();
                    outs.close();
                    soc.close();
                    //
                            break;
                case "5": outs.write("#_#".getBytes());
                    //
                    ins.close();
                    outs.close();
                    soc.close();
                    //
                            break lp;
                default: System.out.println("Invalid selection, please try again");
            }

        }
    }

    /**
     * This function starts the agent
     * @throws IOException
     * @throws InterruptedException
     */

    public void startagent() throws IOException, InterruptedException {
        while(true) {
            System.out.println("What would you like to do? Please select the NUMBER");
            System.out.println("1. Register");
            System.out.println("2. Login");
            System.out.println("3. exit");

            String action = userinp.nextLine();
            int inp;
            String rec = "";


            int flag = 0;
            switch (action) {
                case "1":
                    System.out.println("Enter username");
                    String usnm = userinp.nextLine();
                    System.out.println("Registering");
                    //
                    soc = new Socket("EventManager", 2000);
                    ins = soc.getInputStream();
                    outs = soc.getOutputStream();
                    //
                    outs.write(("reg_" + usnm + "$").getBytes());
                    while ((inp = ins.read()) > 0) {
                        if ((char) inp == '$') {
                            break;
                        }
                        if ((char) inp == 'N') {
                            System.out.println("Already Registered, Please log in with your ID");
                            break;
                        }
                        rec += String.valueOf((char) inp);

                    }
                    System.out.println("If you've been accepted the you will receive a token number below," +
                            " otherwise it is blank");
                    System.out.println("Use this token number, if received, during log in");
                    System.out.println(rec);
                    //
                    ins.close();
                    outs.close();
                    soc.close();
                    //
                    break;


                case "2":
                    System.out.println("Enter your username");
                    String lognm = userinp.nextLine();
                    System.out.println("Enter your token number");
                    String tnum = userinp.nextLine();
                    System.out.println("Logging in");
                    //
                    soc = new Socket("EventManager", 2000);
                    ins = soc.getInputStream();
                    outs = soc.getOutputStream();
                    //
                    outs.write(("log_" + lognm + "_" + tnum + "$").getBytes());
                    while ((inp = ins.read()) > 0) {
                        if ((char) inp == '$') {
                            break;
                        }
                        if ((char) inp == 'N') {
                            System.out.println("You dont have an account, please register");
                            break;
                        } else {
                            System.out.println("Login Successful!");
                            operations(lognm);
                            break;
                        }
                    }

                    //The below section between the opening and closing of connections is for
                    //receiving notifications
                    //
                    soc = new Socket("EventManager", 2000);
                    ins = soc.getInputStream();
                    outs = soc.getOutputStream();

                    outs.write(("H_"+lognm+"$").getBytes());
                    while((inp=ins.read())>0){
                        if((char)inp=='$'){
                            break;
                        }
                        if(!((char)inp=='_')){
                            rec+=String.valueOf((char)inp);
                        }
                        if((char)inp=='_'){
                            notifications.add(rec);
                            rec="";
                        }
                    }
                    notifications.add(rec);
                    rec="";

                    System.out.println("Notifications:");
                    for(int n=0;n<notifications.size();n++) {
                        System.out.println(notifications.get(n));
                    }
                    ins.close();
                    outs.close();
                    soc.close();
                    //
                    break;


                case "3":
                    System.exit(3);
                    break;


                default:
                    System.out.println("Invalid action");
            }
        }
    }

    /**
     * standard main function
     * @param args
     * @throws IOException
     * @throws InterruptedException
     */

    public static void main(String[] args) throws IOException, InterruptedException {
        PubSubAgent start=new PubSubAgent();
        start.startagent();
    }
}
