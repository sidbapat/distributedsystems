package pubsub;

import java.io.IOException;
import java.util.ArrayList;

public interface Subscriber {
    /*
     * subscribe to a topic
     */
    public void subscribe(Topic topic);

    /*
     * subscribe to a topic with matching keywords
     */
    public void subscribe(String keyword) throws IOException;

    /*
     * unsubscribe from a topic
     */
    public void unsubscribe(Topic topic);

    /*
     * unsubscribe to all subscribed topics
     */
    public void unsubscribe(String str) throws IOException;

    /*
     * show the list of topics current subscribed to
     */
    public void listSubscribedTopics() throws IOException;

}
