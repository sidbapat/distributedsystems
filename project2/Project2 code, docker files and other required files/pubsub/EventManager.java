package pubsub;

/*
 * EventManager.java
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * This class demonstrates the working of an event manager of a pubsub distributed system
 *
 * @author Siddharth Bapat
 */

public class EventManager{

    int port=2000; //port n which the ev is listening
    static ArrayList<String> pubsublist=new ArrayList<>();
    static ArrayList<ArrayList<Topic>> subtopiclist=new ArrayList<>();
    static ArrayList<String> eventlist=new ArrayList<>();
    static ArrayList<Topic> availabletopics=new ArrayList<>();
    ServerSocket eventsersoc;
    Socket eventsoc;
    InputStream eventins;
    OutputStream eventouts;

    /*
     * Start the repo service
     */
    private void startService(){
        try {

            eventsersoc=new ServerSocket(port);
            while (true) {
                int req;
                String rec="";
                String snd="";
                String subrec="";
                String subsnd="";
                System.out.println("Server is listening");
                eventsoc = eventsersoc.accept();
                eventins=eventsoc.getInputStream();
                eventouts=eventsoc.getOutputStream();

                while((req=eventins.read())>0){
                    if((String.valueOf((char)req)).equals("$")){
                        break;
                    }
                    rec+=String.valueOf((char)req);
                }
                String[] cmd=rec.split("_");
                switch(cmd[0]){

                    case "reg":
                    if(pubsublist.contains(cmd[1])){
                        eventouts.write("N$".getBytes());
                        System.out.println("Request Rejected");
                    }
                    else{
                        pubsublist.add(cmd[1]);
                        subtopiclist.add(new ArrayList<>());
                        int pos=pubsublist.indexOf(cmd[1]);
                        eventouts.write(String.valueOf(pos).getBytes());
                        System.out.println("Request Accepted");
                    }

                    break;


                    case "log":

                            if (pubsublist.contains(cmd[1])) {
                                eventouts.write("Y$".getBytes());
                                System.out.println("Request Accepted");
                            } else {
                                eventouts.write("N$".getBytes());
                                System.out.println("Request Rejected");
                            }
                            break;


                    case "sub":
                        for (int top = 0; top < availabletopics.size(); top++) {
                            snd += top + 1 + " " + availabletopics.get(top).name+"\n" ;
                        }
                        snd += "$";
                        System.out.println("Sent topics");
                        eventouts.flush();
                        eventouts.write(snd.getBytes());

                        subrec = "";
                        while ((req = eventins.read()) > 0) {
                            if ((char) req == '$') {
                                break;
                            }
                            subrec += String.valueOf((char) req);
                        }
                        addSubscriber(subrec, cmd[1]);
                        eventouts.write("Y$".getBytes());
                        break;


                    case "unsub":
                        int rno=pubsublist.indexOf(cmd[2]);
                        removeSubscriber(rno,cmd[1]);
                        eventouts.write("Y$".getBytes());
                        break;


                    case "pub":
                        String punstr=cmd[1];
                        int ep;
                        eventlist.add(punstr+"_"+cmd[2]);
                        break;


                    case "ad":
                        Topic nadvtop=new Topic(availabletopics.size(),null,cmd[1]);
                        addTopic(nadvtop);
                        break;


                    case "H":
                        System.out.println("Sending notifications");
                        String notuser=cmd[1];

                        String evstr="";
                        int loc=pubsublist.indexOf(notuser);
                        ArrayList<Topic> top=subtopiclist.get(loc);
                        ArrayList<String> topicnames=new ArrayList<>();

                        for(int t=0;t<top.size();t++){
                            topicnames.add(top.get(t).name);
                        }

                        for(int not=0;not<eventlist.size();not++){
                            String ev=eventlist.get(not);
                            String[] sndnot=ev.split("_");
                            if(topicnames.contains(sndnot[1])){
                                evstr+=sndnot[0]+"_";
                            }
                        }
                        evstr+="$";
                        System.out.println(evstr);
                        eventouts.write(evstr.getBytes());
                        break;


                    case "List":
                        int ll=pubsublist.indexOf(cmd[1]);
                        ArrayList<Topic> lst=subtopiclist.get(ll);
                        String sndtoplist="";
                        for(int lt=0;lt<lst.size();lt++){
                            sndtoplist+=lst.get(lt).name+"_";
                        }
                        sndtoplist+="$";
                        eventouts.write(sndtoplist.getBytes());
                        break;


                    default: break;

                }
                rec="";
                System.out.println("Publisher Subscriber List :");
                System.out.println("ID name topics list");
                for(int pr=0;pr<pubsublist.size();pr++){
                    System.out.print(pr+" ");
                    System.out.print(pubsublist.get(pr)+" ");
                    ArrayList<Topic> printt=subtopiclist.get(pr);
                    for(int subp=0;subp<printt.size();subp++){
                        System.out.print(printt.get(subp).name+" ");
                    }
                    System.out.println("");
                }
                System.out.println("");
                System.out.println("Available Events:");
                for(int e=0;e<eventlist.size();e++){
                    System.out.println(eventlist.get(e));
                }
                System.out.println("");
                System.out.println("Available Topics:");
                for(int e=0;e<availabletopics.size();e++){
                    System.out.println(availabletopics.get(e).name);
                }
                System.out.println("");
                eventouts.close();
                eventins.close();
                eventsoc.close();
            }
        }catch (Exception e){
           e.printStackTrace();
        }
    }

    /*
     * notify all subscribers of new event
     */
    private void notifySubscribers(String usr) throws IOException {

    }

    /*
     * add new topic when received advertisement of new topic
     */
    private void addTopic(Topic topic){
        availabletopics.add(topic);
    }

    /*
     * add subscriber to the internal list
     */
    private void addSubscriber(String str,String user){
        Topic tt=null;
        int rollno=pubsublist.indexOf(user);
        for(int i=0;i<availabletopics.size();i++){
            if((availabletopics.get(i).name).equals(str)){
                tt=availabletopics.get(i);
                subtopiclist.get(rollno).add(tt);
                break;
            }
        }

    }

    /*
     * remove subscriber from the list
     */
    private void removeSubscriber(int rn,String tp){
        ArrayList<Topic> remve=subtopiclist.get(rn);
        for(int rm=0;rm<remve.size();rm++){
            if((remve.get(rm).name).equals(tp)){
                remve.remove(rm);
            }
        }
    }

    /*
     * show the list of subscriber for a specified topic
     */
    private void showSubscribers(Topic topic){

    }


    /**
     * standard main function
     * @param args
     */

    public static void main(String[] args) {
        List<String> test=new ArrayList<>();
        availabletopics.add(new Topic(0,new ArrayList(),"football"));
        availabletopics.get(0).keywords.add("man utd");
        new EventManager().startService();
    }


}
