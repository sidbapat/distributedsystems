# E-Newsroom

When the eventmanager code is run, I hardcoded it to include one "football" topic for demonstration purposes.
At the beginning, the eventmanager will have "football" on it's list of available topics.

I did not have to implement any multi-threading. During my testing with 1 eventmanager and 2 agents, the agents were able to 
connect and perform actions correctly.

NOTE: In order to visualise the working of the pubsub system, keep all your terminals side by side and view the updates
in the eventmanager code terminal as you perfrom actions in the pubsubagent terminals.
It should reflect the actions you performed in the pubsubagent terminals.

Everytime an action is performed, the eventmanager will display the list of all users registered.
If any of these users have subscribed, the subscribed topic list will be displayed next to their usernames.
Otherwise nothing is shown. It will also display the published events as well as all the available topics(initially 
only "football" will be on that list).

Please follow the instructions as shown in the terminal. For some of them, you are required to type in the number, and for some
you are required to type in the name of the topic.

Initially you must register yourself, then you must login before performing any action.

Register,Login,Exit: Enter the number
After registering, note down the token number and use it for login

After logging in, you can perform the following actions:
subscribe, unsubscribe, publish, advertise, skip, show subscribed topics. For selecting one of
these actions, type in the number.

```subscribe:```
the terminal will display the list of available topics. For this type in the NAME, exactly as shown
in the terminal.

```unsubscribe:```
for this one, nothing will be displayed, you will only be asked to type in the topic name.
Please note down the topic names displayed during subscribing and remember what you subscribed to
and write down the NAME exactly as it was displayed during the subscribe action.THis will unsubscribe
you from that topic (the eventmanager terminal update will reflect this).

```publish:```
for this one, you'll be asked to type in the event and then asked to type in the topic name associated
with the event. For typing the event, you can type anything that you want to write. For typing in the topic,
please write the same topic NAME exactly as shown earlier.

```advertise:```
for this one, you'll be asked to type in a topic name. You can write whatever you want but
keep in mind and note down what you've written as this name would then be used for the subscription
action exactly as you entered.

```skip:```
This would return to the Register, Login page and from there you can exit the program
by selecting number 3. EVERYTIME YOU SELECT SKIP, YOU WILL SEE ALL THE AVAILABLE EVENTS
OF THE TOPICS YOU SUBSCRIBED UNDER THE NOTIFICATIONS LIST.

```show subscribed topics:```
for this one, when you enter the number for this action, you will see a list of all
subscribed topics under the subscribed topics list.

I did not implement the eventmanager to send notifications to subscribers as soon as it 
recieves it. You will get the notifications list only after you choose skip action (type in 5)

I did not implement the eventmanager to send all advertised topics immediately to the agents.
Whenever you choose to subscribe and the list of topics are displayed, it will show all the 
latest availbale topics.

I did not directly implement any QoS as I used TCP socket programming.


# Execution Instructions:
All the files required for this project (Project2) that needed to be updated such as Dockerfile, 
docker-compse.yml,and java codes. are inside the project2 directory. 
Do not use any dockerfile or docker-compose file or java codes from anywhere else. Only the other
files that where required for project1 but not changed in any way for this project can
be reused.

Just like for Project1, I used Docker Toolbox for this project.

Prior to running the codes in Docker, I had manually created a folder/directory in my system 
that included the java codes, Dockerfile, docker-compose.yml and any other file such as
LICENSE, pom,.gitignore,and any other file required for execution.

The main folder included the following:
basic_word_count, distributed_consensus, distributed_hash_table, project1, project2,
pubsub, remote_procedure_call, socket_programming folders along with .gitignore, docker-compose,
Dockerfile, LICENSE, pom, and README.md


For project1 execution, my folder had copied everything from the main page of gitlab.
For project2, I used the same contents of the folder, except the Dockerfile, docker-compose.yml,
and the java codes are different and updated as required. For this project,in the main folder I created
a project2 folder that contains the package folder(pubsub) which in turn contains the java codes.

Please ensure you have all the required files in the main folder.

Remove modifications done for project1 and update your Dockerfile to include:
```
COPY project2 /csci652/project2
```
If you need to copy anymore files, you may need to update the Dockerfile to include:
```
COPY project1/<filename> /csci652/project1
```
However I never used it as I had manually copied all the files in advance.

Modify the docker-compose.yml file. I changed the container names from project1.
The main container is named as "EventManager" and other containers that depend on it 
are named as "PubSubAgent1", "PubSubAgent2", and so on. The provided docker-compose file
inlcudes EventManager and 2 more containers "PubSubAgent1" and "PubSubAgent2". You may had more
containers to the docker-compose file by repeating the code block for one of the 2 containers 
and change it's name appropriately.

In my code, I hardcoded the eventmanager container as "EventManager". So use this name for the main
container and for other containers use "PubSubAgent1","PubSubAgent2", and so on with only the number being different.


Open the Docker Toolbox Quickstart and use "cd" commands to traverse to your main folder

Execute the following:
```
docker-compose build
```
-f is not needed because I ensured only one instance of correct Dockerfile and docker-compose.yml
files are included in the folder.

During my execution, for PubSubAgent1 and PubSubAgent2, it was stated that both of them
"uses an image, skipping". It should not matter for you but if it does, you may need to adjust
your system settings/status.

Execute the following:
```
docker-compose up -d
```

After the above succeeds, run the following:
EventManager is the container name.
```
docker exec -it EventManager bash
```

use "cd" to traverse to project2
```
cd project2
```

Then run the EventManager.java code,assuming pubsub is the package name:
```
javac pubsub/EventManager.java
java pubsub/EventManager
```

You may get some notes/warnings during compilation that can be ignored.

Open another Toolbox Quickstart terminal, use "cd" commmands to traverse to your main folder
and directly run the following: PubSubAgent1 is the container name.
```
docker exec -it PubSubAgent1 bash
```

use "cd" to traverse to project2
```
cd project2
```

Then run the agent codes,assuming pubsub is the package name:
```
javac pubsub/PubSubAgent.java
java pubsub/PubSubAgent
```
You may get some notes/warnings during compilation that can be ignored.

Repeat the same for more PubSubAgent containers. Write appropriate names for the container name. 

Use the below commands to delete/clean up after you're done:
```
docker-compose down
```



