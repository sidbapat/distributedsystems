package Kademlia;

/*
 * KademliaPeer.java
 */

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class demonstrates the working of a peer node in a kademlia distributed system
 *
 * @author          Siddharth Bapat
 */

public class KademliaPeer extends Thread{
    enum STATUS {
        ONLINE, OFFLINE;
    }

    //variables for socket programming
    Socket nodesocket;
    InputStream nodeinp;
    OutputStream nodeout;
    Scanner userinp;

    //message storage
    ArrayList<String> files=new ArrayList<>();

    //routing table for each peer
    ArrayList<String> binaries=new ArrayList<>();
    ArrayList<Integer> nodeforbin=new ArrayList<>();

    //total max number of nodes in the kademlia
    //This value is always expected to be a power of 2
    public static int numberofnodes;

    //current peer ID
    public static int currentnodeid;

    //current peer IP and port
    static String nodeip;
    static String nodeport;

    //IP and port of the MiniServer
    private String ipAddress="Miniserver";//Config.DEFAULT_PEER_IP;
    private int port=Config.DEFAULT_PORT_PREFIX;

    /**
     * This function returns the fileID for the file
     * In this case, the number of characters of the string
     * modulo the total max number of peers
     *
     * @param       msg     string message
     * @return              fileID
     */

    public int calculatefileID(String msg){
        return msg.length()%numberofnodes;
    }

    /**
     * This function generates the 4 bit binary value for the integer value
     * @param       n       integer value
     * @return              binary string
     */

    public String generate4bit(int n){
        String num=Integer.toBinaryString(n);

        //routing table size-1 is by default the max binary size
        if(num.length()<binaries.size()-1) {
            String temp = "";
            int diff = (binaries.size() - 1) - num.length();
            for (int a = 0; a < diff; a++) {
                temp += "0";
            }
            temp += num;
            num = temp;
            return num;
        }
        else{
            return num;
        }
    }

    /**
     * This function updates the routing table according to the
     * available nodes
     * @param       avnds       String of available nodes
     */

    public void updateroutingtable(String avnds){
        //records nodes already considered
        ArrayList<Integer> selected=new ArrayList<>();
        //updates only the entries after "itself"
        for(int i=1;i<binaries.size();i++){
            //considers the current node options for an entry in the table
            ArrayList<Integer> options=new ArrayList<>();
            selected.add(currentnodeid);
            String obin=binaries.get(0);  //original binary string
            String pref=obin.substring(0,(binaries.size()-1)-i);
            String tp=pref;

            //determine the possible node options for each entry
            for(int j=0;j<i;j++){
                tp+="0";
            }
            int start=Integer.parseInt(tp,2);
            tp=pref;
            for(int j=0;j<i;j++){
                tp+="1";
            }
            int end=Integer.parseInt(tp,2);

            //find out which nodes are online
            for(int k=start;k<=end;k++){
                if(k!=currentnodeid) {
                    if (avnds.contains("" + k)) {
                        if(!selected.contains(k)) {
                            options.add(k);
                        }
                    }
                    selected.add(k);
                }
            }

            //from the options select the node that returns the smallest XOR value
            if(options.size()>0) {
                int ans =currentnodeid ^ options.get(0);
                int loc=0;
                for (int ch = 1; ch < options.size(); ch++) {
                    if(ans>(currentnodeid ^ options.get(ch))){
                        ans=currentnodeid ^ options.get(ch);
                        loc=ch;
                    }
                }
                nodeforbin.set(i,options.get(loc));
            }
            else{
                nodeforbin.set(i,-1);
            }
        }
        printtable();
    }

    /**
     * This function prints the routing table
     */

    public void printtable(){
        System.out.println("Routing Table");
        for(int p=0;p<binaries.size();p++){
            System.out.print(binaries.get(p)+" : ");
            System.out.println(nodeforbin.get(p));
        }
    }

    /**
     * This function sets up the initial routing table when a node
     * joins the system.
     */

    public void routingtableinitialsetup(){
        int entries=0;
        int nodecount=numberofnodes;

        while(nodecount!=1){
            nodecount=nodecount/2;
            entries+=1;
        }

        //sets binary to four digits
        //adds itself as the first entry of the table
        String originalbinary=Integer.toBinaryString(currentnodeid);
        if(originalbinary.length()<entries){
            String temp="";
            int diff=entries-originalbinary.length();
            for(int a=0;a<diff;a++){
                temp+="0";
            }
            temp+=originalbinary;
            originalbinary=temp;
        }
        binaries.add(originalbinary);
        nodeforbin.add(currentnodeid);

        //adds the remaining entries with an initial value of -1 to denote "null"
        for(int i=0;i<entries;i++){
            String bin=originalbinary.substring(0,entries-(i+1));

            String temps=bin;
            for(int j=0;j<i+1;j++){
                temps+="X";
            }
            binaries.add(temps);
            nodeforbin.add(-1);

        }
    }

    /**
     * This function sends a request to the miniserver to find out which
     * nodes are currently available
     * @param       ins     inputstream
     * @param       outs    outputstream
     * @return              String of available nodes
     * @throws IOException
     */

    public String findnodes(InputStream ins,OutputStream outs) throws IOException {
        System.out.println("Checking available nodes.......");
        outs.write(("FINDNODES_$").getBytes());
        System.out.println("Available nodes");
        int n=0;
        String avail="";
        while((n=ins.read())>0){
            if((char)n=='$'){
                break;
            }
            System.out.print((char)n);
            avail+=(char)n;
        }
        System.out.println("");
        return avail;
    }

    /**
     * This function sends a request to the miniserver for the IP and port number of
     * the node whose ID was sent as part of the request
     * @param       ins     inputstream
     * @param       outs    outputstream
     * @param       nd      node ID whose IP and port is needed
     * @return              String containing the IP and port
     * @throws IOException
     */

    public String getIPandport(InputStream ins,OutputStream outs, int nd) throws IOException {
        System.out.println("Retrieving IP and port of next peer");
        outs.write(("REQIP_"+nd+"$").getBytes());
        String info="";
        int n=0;
        while((n=ins.read())>0){
            if((char)n=='$'){
                break;
            }
            info+=(char)n;

        }
        return info;
    }

    /**
     * This function registers the node to the system whenever it comes online
     * @param       ip      node IP
     * @param       pt      node port
     */

    public void connect(String ip,String pt){
        try{
            nodesocket=new Socket(ipAddress,port);
            nodeinp=nodesocket.getInputStream();
            nodeout=nodesocket.getOutputStream();
            nodeout.write(("POST"+"_"+ip+"_"+pt+"$").getBytes());
            int n=0;
            String id="";
            while((n=nodeinp.read())>0){
                if((char)n=='$'){
                    break;
                }
                System.out.print((char)n);
                id+=(char)n;

            }
            id=id.substring(id.length()-2);

            //note down the node ID generated by the miniserver
            currentnodeid=Integer.parseInt(id);
            System.out.println("");
            nodeinp.close();
            nodeout.close();
            nodesocket.close();
            System.out.println("Node ID:"+currentnodeid);

            //set up initial routing table
            routingtableinitialsetup();
            printtable();

            //begin the main operations
            loop:
            {
                while (true) {
                    System.out.println("What would you like to do? Please select the task number ");
                    System.out.println("1. Insert file");
                    System.out.println("2. Retrieve file");
                    System.out.println("3. Exit");
                    System.out.println("4. See stored messages");
                    userinp = new Scanner(System.in);
                    String num = userinp.nextLine();
                    switch (num) {

                        //inserting a file
                        case "1":   nodesocket=new Socket(ipAddress,port);
                            nodeinp=nodesocket.getInputStream();
                            nodeout=nodesocket.getOutputStream();
                            System.out.println("Enter the message");
                            num=userinp.nextLine();
                            String msg=num;
                            int fileid=calculatefileID(num);
                            System.out.println("Your file ID is: "+fileid);
                            String availablenodes="";
                            //if file ID is same as current node ID, add it to itself
                            if(fileid==currentnodeid){
                                files.add(num);
                                System.out.println("File stored in your own system");
                            }
                            else{
                                int flag=0;
                                availablenodes=findnodes(nodeinp,nodeout);

                                nodeinp.close();
                                nodeout.close();
                                nodesocket.close();

                                updateroutingtable(availablenodes);

                                //find the longest prefix match
                                int nid=-1;
                                for(int l=1;l<binaries.size();l++){
                                    String sub=binaries.get(l).substring(0,binaries.get(l).length()-l);
                                    for(int d=0;d<l;d++){
                                        sub+=".";
                                    }
                                    Pattern p=Pattern.compile(sub);
                                    Matcher m=p.matcher(generate4bit(fileid));

                                    if(m.find()){
                                        if(nodeforbin.get(l)==-1){
                                            nid=findclosestXOR(fileid);
                                            flag=1;
                                            break ;
                                        }
                                        else {
                                            nid = nodeforbin.get(l);
                                            break;
                                        }
                                    }
                                }

                                nodesocket=new Socket(ipAddress,port);
                                nodeinp=nodesocket.getInputStream();
                                nodeout=nodesocket.getOutputStream();

                                System.out.println("Attempting to insert file......");
                                String info=getIPandport(nodeinp,nodeout,nid);

                                String[] sp=info.split("_");
                                System.out.println("The peer IP: "+sp[0]);
                                System.out.println("The peer Port: "+sp[1]);

                                nodeinp.close();
                                nodeout.close();
                                nodesocket.close();

                                nodesocket=new Socket(sp[0],Integer.parseInt(sp[1]));
                                nodeinp=nodesocket.getInputStream();
                                nodeout=nodesocket.getOutputStream();

                                if(flag==0) {
                                    //normal insertion
                                    nodeout.write(("INS_" + msg + "_" + fileid + "$").getBytes());
                                }
                                else{
                                    //insertion usually when target is offline
                                    nodeout.write(("EINS_" + msg + "_" + fileid + "$").getBytes());
                                }
                            }
                            nodeinp.close();
                            nodeout.close();
                            nodesocket.close();
                            break;

                        //retrieving a file
                        case "2":   String availnodes="";
                            System.out.println("Enter file ID");
                            int fid=Integer.parseInt(userinp.nextLine());

                            nodesocket=new Socket(ipAddress,port);
                            nodeinp=nodesocket.getInputStream();
                            nodeout=nodesocket.getOutputStream();

                            availnodes=findnodes(nodeinp,nodeout);

                            nodeinp.close();
                            nodeout.close();
                            nodesocket.close();

                            updateroutingtable(availnodes);

                            if(fid==currentnodeid){
                                for(int p=0;p<files.size();p++){
                                    System.out.println(files.get(p));
                                }
                            }
                            else{
                                int flag=0;
                                int nid=-1;
                                for(int l=1;l<binaries.size();l++){
                                    String sub=binaries.get(l).substring(0,binaries.get(l).length()-l);
                                    for(int d=0;d<l;d++){
                                        sub+=".";
                                    }
                                    Pattern p=Pattern.compile(sub);
                                    Matcher m=p.matcher(generate4bit(fid));

                                    if(m.find()){
                                        if(nodeforbin.get(l)==-1){
                                            nid=findclosestXOR(fid);
                                            flag=1;
                                            break ;
                                        }
                                        else {
                                            nid = nodeforbin.get(l);
                                            break;
                                        }
                                    }
                                }

                                nodesocket=new Socket(ipAddress,port);
                                nodeinp=nodesocket.getInputStream();
                                nodeout=nodesocket.getOutputStream();


                                System.out.println("Attempting to retrieve file.........");
                                String info=getIPandport(nodeinp,nodeout,nid);

                                nodeinp.close();
                                nodeout.close();
                                nodesocket.close();

                                String[] sp=info.split("_");

                                nodesocket=new Socket(sp[0],Integer.parseInt(sp[1]));
                                nodeinp=nodesocket.getInputStream();
                                nodeout=nodesocket.getOutputStream();

                                if(flag==0) {
                                    //normal retrieval. the ip and port is used by the final node to directly send
                                    //back the files to the source
                                    nodeout.write(("RET_" + fid + "_" + nodeip + "_" + nodeport + "$").getBytes());
                                }
                                else{
                                    //retrieval similar to insertion usually when target node is offline
                                    nodeout.write(("ERET_" + fid + "_" + nodeip + "_" + nodeport + "$").getBytes());
                                }
                            }
                            nodeinp.close();
                            nodeout.close();
                            nodesocket.close();
                            break;

                        //disconnect from the system after passing stored data if the node stores it
                        case "3":   nodesocket=new Socket(ipAddress,port);
                            nodeinp=nodesocket.getInputStream();
                            nodeout=nodesocket.getOutputStream();
                            System.out.print("Enter your ID");
                            num=userinp.nextLine();

                            //transfer files to the next closest node before exiting
                            if(files.size()>0){
                                String passmsg="";
                                for(int i=0;i<files.size();i++) {
                                    passmsg+=files.get(i)+"\n";
                                }
                                int passfileid=calculatefileID(passmsg);

                                //after passing the files, the file ID changes and it includes all files together
                                //this must be noted down by the user. The system does not record it
                                System.out.println("The ID of the entire message being moved is: "+passfileid);

                                String nds=findnodes(nodeinp,nodeout);
                                updateroutingtable(nds);
                                int passid=findclosestXOR(passfileid);
                                System.out.println(passid);

                                nodeinp.close();
                                nodeout.close();
                                nodesocket.close();

                                nodesocket=new Socket(ipAddress,port);
                                nodeinp=nodesocket.getInputStream();
                                nodeout=nodesocket.getOutputStream();

                                String dest=getIPandport(nodeinp,nodeout,passid);
                                String[] add=dest.split("_");


                                nodeinp.close();
                                nodeout.close();
                                nodesocket.close();

                                nodesocket=new Socket(add[0],Integer.parseInt(add[1]));
                                nodeinp=nodesocket.getInputStream();
                                nodeout=nodesocket.getOutputStream();

                                nodeout.write(("EINS_"+passmsg+"_"+passid+"$").getBytes());

                                nodeinp.close();
                                nodeout.close();
                                nodesocket.close();

                            }

                            nodesocket=new Socket(ipAddress,port);
                            nodeinp=nodesocket.getInputStream();
                            nodeout=nodesocket.getOutputStream();

                            nodeout.write(("EXIT_"+num+"$").getBytes());
                            while((n=nodeinp.read())>0){
                                if((char)n=='$'){
                                    break;
                                }
                                System.out.print((char)n);
                            }
                            nodeinp.close();
                            nodeout.close();
                            nodesocket.close();
                            break loop;

                        //display all stored files
                        case "4":   System.out.println("The stored messages are:");
                                    for(int m=0;m<files.size();m++){
                                        System.out.println(files.get(m));
                                    }
                                    break;

                        default:    System.out.println("Invalid choice, please try again");
                                    break;
                    }

                }
            }
            System.exit(3);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This function finds the closest node from the routing table based on XOR
     * @param       fno     the id, either the node ID or the file ID
     * @return              the closest node ID
     */

    public int findclosestXOR(int fno){
        //find node ID
        int near=1000;
        int temp=0;
        int pos=0;
        for(int i=0;i<nodeforbin.size();i++){
            if(nodeforbin.get(i)>=0) {
                temp = fno ^ nodeforbin.get(i);
                if(temp<near){
                    near=temp;
                    pos=i;
                }
            }
        }
        return nodeforbin.get(pos);
    }

    /**
     * In addition to the main operations mentioned earlier, the other operations such as
     * passing messages to other nodes, displaying the messages retrieved back etc.
     * are managed by this function
     */

    public void run(){
        try {
            ServerSocket servsoc=new ServerSocket(Integer.parseInt(nodeport),50, InetAddress.getByName(nodeip));
            while (true){
                Socket seracc=servsoc.accept();
                InputStream serin=seracc.getInputStream();
                OutputStream serout=seracc.getOutputStream();
                int s;
                String f="";
                while((s=serin.read())>0){
                    if((char)s=='$'){
                        break;
                    }
                    f+=(char)s;

                }
                String[] action=f.split("_");
                if(action[0].equals("FINMSG")){   //displays the messages received

                    System.out.println("=======================================================================");
                    System.out.println("NOTIFICATION :");
                    System.out.println("Message is: ");
                    System.out.print(action[1]);
                    System.out.println("");
                    System.out.println("=======================================================================");

                    serin.close();
                    serout.close();
                    seracc.close();

                    continue;
                }

                if(action[0].contains("INS")) {  //insertion operation
                    int fid=Integer.parseInt(action[2]);
                    if(fid==currentnodeid || action[0].equals("EINS")) {  //store the messages
                        files.add(action[1]);
                        f = "";
                    }
                    else {

                        serin.close();
                        serout.close();
                        seracc.close();

                        seracc=new Socket(ipAddress,port);
                        serin=seracc.getInputStream();
                        serout=seracc.getOutputStream();

                        String availablenodes=findnodes(serin,serout);
                        updateroutingtable(availablenodes);

                        serin.close();
                        serout.close();
                        seracc.close();

                        //find the matching node
                        int flag=0;
                        int nid=-1;
                        for(int l=1;l<binaries.size();l++){
                            String sub=binaries.get(l).substring(0,binaries.get(l).length()-l);
                            for(int d=0;d<l;d++){
                                sub+=".";
                            }
                            Pattern p=Pattern.compile(sub);
                            Matcher m=p.matcher(generate4bit(fid));

                            if(m.find()){
                                if(nodeforbin.get(l)==-1){
                                    nid=findclosestXOR(fid);
                                    flag=1;
                                    break ;
                                }
                                else {
                                    nid = nodeforbin.get(l);
                                    break;
                                }
                            }
                        }

                        //get IP and port and pass to the next node
                        seracc = new Socket(ipAddress, port);
                        serin = seracc.getInputStream();
                        serout = seracc.getOutputStream();

                        int n=0;
                        System.out.println("=======================================================================");
                        System.out.println("NOTIFICATION :");
                        System.out.println("Attempting to retrieve file.........");

                        String info=getIPandport(serin,serout,nid);
                        System.out.println("=======================================================================");

                        serin.close();
                        serout.close();
                        seracc.close();


                        String[] sp = info.split("_");
                        seracc = new Socket(sp[0], Integer.parseInt(sp[1]));
                        serin = seracc.getInputStream();
                        serout = seracc.getOutputStream();

                        if(flag==0) {// normal insertion
                            serout.write(("INS_"+action[1]+"_"+fid+"$").getBytes());
                        }
                        else{//usually when target is offline
                            serout.write(("EINS_"+action[1]+"_"+fid+"$").getBytes());
                        }
                        System.out.println("=======================================================================");
                        System.out.println("NOTIFICATION :");
                        System.out.println("A file has been passed");
                        System.out.println("=======================================================================");
                    }
                }

                //file retrieval operation
                else{
                    int fid=Integer.parseInt(action[1]);
                    if(fid==currentnodeid || action[0].equals("ERET")){//send the stored messages back
                        String messages="";
                        for(int m=0;m<files.size();m++){
                            messages+=files.get(m)+"\n";
                        }

                        serin.close();
                        serout.close();
                        seracc.close();

                        seracc=new Socket(action[2],Integer.parseInt(action[3]));
                        serin=seracc.getInputStream();
                        serout=seracc.getOutputStream();

                        serout.write(("FINMSG_"+messages+"$").getBytes());
                        System.out.println("=======================================================================");
                        System.out.println("NOTIFICATION :");
                        System.out.println("Sent a file back to the source");
                        System.out.println("=======================================================================");

                    }
                    else{
                        String replyip=action[2];
                        String replyport=action[3];

                        serin.close();
                        serout.close();
                        seracc.close();

                        seracc=new Socket(ipAddress,port);
                        serin=seracc.getInputStream();
                        serout=seracc.getOutputStream();

                        String availablenodes=findnodes(serin,serout);
                        updateroutingtable(availablenodes);

                        serin.close();
                        serout.close();
                        seracc.close();

                        //find the matching node
                        int flag=0;
                        int nid=-1;
                        for(int l=1;l<binaries.size();l++){
                            String sub=binaries.get(l).substring(0,binaries.get(l).length()-l);
                            for(int d=0;d<l;d++){
                                sub+=".";
                            }
                            Pattern p=Pattern.compile(sub);
                            Matcher m=p.matcher(generate4bit(fid));

                            if(m.find()){
                                if(nodeforbin.get(l)==-1){
                                    nid=findclosestXOR(fid);
                                    flag=1;
                                    break ;
                                }
                                else {
                                    nid = nodeforbin.get(l);
                                    break;
                                }
                            }
                        }

                        seracc = new Socket(ipAddress, port);
                        serin = seracc.getInputStream();
                        serout = seracc.getOutputStream();

                        int n=0;
                        System.out.println("=======================================================================");
                        System.out.println("NOTIFICATION :");
                        System.out.println("Attempting to retrieve file.........");

                        String info=getIPandport(serin,serout,nid);

                        serin.close();
                        serout.close();
                        seracc.close();

                        String[] sp = info.split("_");
                        seracc = new Socket(sp[0], Integer.parseInt(sp[1]));
                        serin = seracc.getInputStream();
                        serout = seracc.getOutputStream();

                        if(flag==0) {//normal retrieval
                            serout.write(("RET_"+fid+"_"+replyip+"_"+replyport+"$").getBytes());
                        }
                        else{//retrieval when target is offline
                            serout.write(("ERET_"+fid+"_"+replyip+"_"+replyport+"$").getBytes());
                        }
                        System.out.println("A file has been passed");
                        System.out.println("=======================================================================");
                    }
                }
                serin.close();
                serout.close();
                seracc.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Standard main function
     * @param   args    The node IP address and the MAX possible number of nodes in the system
     */

    public static void main(String[] args){
        System.out.println("Starting this node");
        nodeip=args[0];
        numberofnodes=Integer.parseInt(args[1]);
        nodeport="1000";
        KademliaPeer obj=new KademliaPeer();
        obj.start();
        obj.connect(nodeip,nodeport);
    }
}
