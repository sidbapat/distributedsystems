package Kademlia;

/*
 * MiniServer.java
 */
import java.awt.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class represents a peer object
 */
class PeerInfo {
    private String ipAddress;
    private String port;
    public PeerInfo(String ipAddress, String port){
        this.ipAddress = ipAddress;
        this.port = port;
    }

    public String getIpAddress(){ return this.ipAddress; }
    public String getPort(){ return this.port; }
}

/**
 * This class represents the working of a miniserver that keeps track of all the peers
 * in the system and provides the appropriate addresses of the nodes to the nodes requesting it
 *
 * @author Siddharth Bapat
 */

public class MiniServer {
    private static Map<Integer, PeerInfo> onlinePeers = new HashMap<Integer, PeerInfo>();
    private static int numberofnodes;

    public static void addPeer(int peerID, String ipAddres, String port) {
        onlinePeers.put(peerID, new PeerInfo(ipAddres, port));
    }

    public static void removePeer(int peerID) {
        onlinePeers.remove(peerID);
    }

    public static boolean isPeerOnline(int peerID) {
        return onlinePeers.containsKey(peerID);
    }

    /**
     * The port that the server listens on.
     */
    private static final int PORT = Integer.parseInt(Config.SERVER_PORT);

    /**
     * A handler thread class.  Handlers are spawned from the listening
     * loop and are responsible for a dealing with a single client
     * and broadcasting its messages.
     */
    private static class Handler extends Thread {
        private String name;
        private Socket socket;
        private InputStream in;
        private OutputStream out;
        //private Dispatcher dispatcher;

        /**
         * Constructs a handler thread, squirreling away the socket.
         * All the interesting work is done in the run method.
         */
        public Handler(Socket socket) {
            this.socket = socket;

            // Create a new JSON-RPC 2.0 request dispatcher
           // this.dispatcher =  new Dispatcher();

            // Register the "echo", "getDate" and "getTime" handlers with it
            //dispatcher.register(new JsonHandler.MiniServerHandler());
        }

        /**
         * Services this thread's client by repeatedly requesting a
         * screen name until a unique one has been submitted, then
         * acknowledges the name and registers the output stream for
         * the client in a global set, then repeatedly gets inputs and
         * broadcasts them.
         */
        public void run() {
            try {
                // Create character streams for the socket.
                in = socket.getInputStream();
                out = socket.getOutputStream();
                System.out.println("Received request");
                // read request
                String line="";

                int content= 0;
                while((content=in.read())>0){
                    if((char)content=='$'){
                        break;
                    }
                    line+=(char)content;
                }
                System.out.println(line);
                String[] req=line.split("_");
                int hashvalue=-1;
                switch(req[0]){
                    //records the nodes joining the system
                    case "POST":    //String[] ipadd=req[1].split("\\.");
                                    //int value=Integer.parseInt(ipadd[ipadd.length-1]);
                                    //hashvalue=(value-2)%numberofnodes;

                                    String n=req[1].substring(req[1].length()-1);
                                    String m=n.substring(n.length()-1);
                                    if(m.equals("r")) {
                                        hashvalue = Integer.parseInt(n);
                                    }
                                    else{
                                        String i=m+n;
                                        hashvalue=Integer.parseInt(i);
                                    }
                                    addPeer(hashvalue,req[1],req[2]);
                                    out.write(("You are connected \n Your ID is 0"+hashvalue+"$").getBytes());
                                    hashvalue=-1;
                                    System.out.println("Added node into the system");
                                    break;

                    //removes the node from the system
                    case "EXIT":    int id=Integer.parseInt(req[1]);
                                    removePeer(id);
                                    out.write("You are disconnected".getBytes());
                                    break;

                    //returns information on all online nodes
                    case "FINDNODES":   ArrayList<Integer> onlinenodeIDs=new ArrayList<>(onlinePeers.keySet());
                                        String onln="";
                                        for(int i=0;i<onlinenodeIDs.size();i++){
                                            onln+=onlinenodeIDs.get(i)+" ";
                                        }
                                        out.write(onln.getBytes());
                                        System.out.println("Sent list of available nodes");
                                        break;

                    //sends the IP and port of the node requested
                    case "REQIP":   String ip=onlinePeers.get(Integer.parseInt(req[1])).getIpAddress();
                                    String pt=onlinePeers.get(Integer.parseInt(req[1])).getPort();
                                    out.write((ip+"_"+pt+"$").getBytes());
                                    System.out.println("Sent IP and port numbers");
                                    break;

                    default:        out.write("INVALID REQUEST".getBytes());
                    break;
                }

                //displays the available nodes
                System.out.println("Nodes available");
                System.out.println(onlinePeers.keySet());

                out.flush();
                out.close();
                socket.close();
            } catch (IOException e) {
                System.out.println(e);
            /*} catch (JSONRPC2ParseException e) {
                e.printStackTrace();*/
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                }
            }
        }
    }

    /**
     * Standard main function
     * @param       args        the max number of nodes in the system
     * @throws Exception
     */

    public static void main(String[] args) throws Exception {

        System.out.println("The server is running.");
        ServerSocket listener = new ServerSocket(PORT);
        numberofnodes=Integer.parseInt(args[0]);
        try {
            while (true) {
                new Handler(listener.accept()).start();


            }
        } finally {
            listener.close();
        }
    }


}
