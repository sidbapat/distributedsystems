package Kademlia;

public class Config {
    public static final String SERVER_IP = "127.0.0.1";
    public static final String SERVER_PORT = "8080";

    public static final String DEFAULT_PEER_IP = "127.0.0.1";
    public static final int DEFAULT_PORT_PREFIX = 8080;
}
