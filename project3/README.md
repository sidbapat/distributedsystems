# Dynamic Share

```Some points to note:```
1. In the interaction diagram, it was stated that each peer would maintain a hashmap. However, a slight modification was done in the actual project. Instead of hashmap, I
used 2 synchronised arraylists to design the routing table as the entries needed to be in order. Hashmap would have used random ordering so I used arraylists instead.
2. Throughout this project, all the hash functions used are merely to replicate the real system. The hash functions used here are simple and straightforward and just to imitate the real system
3. Initially the peer IP address would be fed into the program and the miniserver would generate node IDs based on it. eg 127.0.0.2 --> Node 0 using the simple concept of taking the last
number in the IP address , i,e "2" and then subtracting 2 to give 0. Eg 127.0.0.4 --> 4-2 = Node ID 2. However this does not work in Docker as I need to pass the container names so I changed
it into Peer0 --> Node 0, Peer4 --> Node 4 and so on for simplicity. The original hash function is commented out.
4. The Files are assumed to remain wherever they are. If they are stored in another node due to the unavailability of the ideal node. The files remain there itself. They DO NOT move to the ideal node when it joins.
5. However, when a node leaves, it passes its files to the nearest node. The files MOVE in this case.
6. I have not implemented any unique identification for the files other than the file ID. Therefore whenever a file is requested, the node that passes the file to the calling node,
passes ALL of it's file contents to the source along with the file it needs. This is like a service system where when the user enters a keyword, the system displays a number of options
which the user can select. This imitates that.
7. When the node exits and passes its contents to another node, it generates a NEW FILE ID for this collection. THIS MUST BE NOTED FOR FUTURE RETRIEVAL.
8. Throughout the project, all IDs must be remembered and noted down. The system does not keep records for users' reference.

When you run the miniserver, you have to provide the max possible number of nodes in the system. In this case it is 16.
The MiniServer keeps track of all the nodes online in the system and only performs the following operations as required:
1. Register nodes entering and leaving the system and maintain the hashmap accordingly
2. Send a list of available nodes wheneever requested
3. Send IP address(PeerX) and port of the node requested by a node for some operation

When you run the Peer, the input is the "PeerX" (where X is a number which would become it's node ID) as well as the max possible number of nodes in the system. This number
is usually a power of 2. In this project it is 16. Throughout this project node ID of "-1" in the routing table means that entry is empty (no nodes online for that entry)

Once it is running, it will register itself with the miniserver and initially set up it's routing table. The node ID will be displayed. YOU MUST NOTE IT DOWN. You will have 4 options to choose:

```NOTE:```
1. Before every operation, the node updates the routing table. There is no heartbeat or any regular calls. The routing table is updated before any operation takes place.
2. In each peer, 2 threads are active. The main thread takes care of the main operations that the user needs to select as mentioned below. The other thread remains active and handles
operations due to the main operation such as receiving and storing files, passing files to other nodes etc. (background operations) while simultaneously being online to handle
the main operations the user selects. Throughout the demonstration, anything that appears as a notification within an upper and lower "====" boundaries is the result of background
operations mentioned. Other outputs are the main operations.

MAIN OPERATIONS:

```Insert file:``` 
Here, you select "1" then you will be prompted to enter a message string. Once you enter the string, the hash function here counts the number of characters in the string (string length)
and then finds the modulus of it with the max number of nodes (here it is 16) and that value is the file ID. YOU MUST NOTE DOWN AND REMEMBER THE FILE ID. If the file ID is the same as the current node ID, the file is then stored
within the current node. Otherwise the node checks the routing table for a node of the same ID. If it finds a match and the node is online, it will send it to that node. If it is not
online, then it searches the routing table and xors the file ID with all the nodes in its routing table, including itself and then stores it in that node. When it needs to send a file
to another node, it requests the miniserver for the IP and port of that node and then uses this to establish a connection with that node and send the file to it.

```Retrieve file:```
Here, you will be prompted to enter the file ID of the file you want to retrieve. If the file is present, you will receive a notification within the "====" boundries with the message.
The operations performed here are similar to the insertion operation. It finds the appropriate node and sends the request along with its own IP and port. Whichever node contains that file
would use this IP and port to establish a direct connection with the source node and send the file back. All of this happens in the background by the second thread and all notifications
of these operations would appear under "===" boundaries.

```Exit:```
Here, the node transfers all its files to the nearest nodes according to the routing table before it leaves the system. You will first be prompted to enter the node ID. YOU MUST HAVE NOTED IT DOWN
IN THE BEGINNING. When the node passes its files, it passes ALL the stored files collectively and generates a NEW FILE ID for that collection. It displays that ID and YOU MUST NOTE IT DOWN.


```See stored messages:```
Here, the node displays all the messages stored in itself. This is for demonstration purposes.


BACKGROUND OPERATIONS:

Another thread maintains the background operations occuring due to the above main operations. It is another server socket that waits for a separate request from the nodes. Special commands
are present in the requests that enable it to insert, pass on, retrieve files and send back to the source etc. accordingly. Special operations such as inserting into a non-ideal node, passing only one node
ahead and stop there, retrieving its own files and passing them back to the source node using that nodes IP and port which is received as part of the request is taken care of by the special commands
in the request accordingly. Whereever these operations are being performed, the node performing the operation displays the notification messages under the "===" boundaries.



# Execution Instructions:
All the files required for this project (Project3) that needed to be updated such as Dockerfile, 
docker-compse.yml,and java codes. are inside the project3 directory. 
Do not use any dockerfile or docker-compose file or java codes from anywhere else. Only the other
files that where required for project1 but not changed in any way for this project can
be reused.

Just like for Project2, I used Docker Toolbox for this project.

Prior to running the codes in Docker, I had manually created a folder/directory in my system 
that included the java codes, Dockerfile, docker-compose.yml and any other file such as
LICENSE, pom,.gitignore,and any other file required for execution.

The main folder included the following:
basic_word_count, distributed_consensus, distributed_hash_table, project1, project2,project3,
pubsub, remote_procedure_call, socket_programming folders along with .gitignore, docker-compose,
Dockerfile, LICENSE, pom, and README.md


For project1 execution, my folder had copied everything from the main page of gitlab.
For project2, I used the same contents of the folder, except the Dockerfile, docker-compose.yml,
and the java codes are different and updated as required. For this project,in the main folder I created
a project2 folder that contains the package folder(pubsub) which in turn contains the java codes.
I followed the same pattern appropriately for project 3 as I did for project 2.

Please ensure you have all the required files in the main folder.

Remove modifications done for project2 and update your Dockerfile to include:
```
COPY project3 /csci652/project3
```
If you need to copy anymore files, you may need to update the Dockerfile to include:
```
COPY project3/<filename> /csci652/project3
```
However I never used it as I had manually copied all the files in advance.

Modify the docker-compose.yml file. I changed the container names from project2.
The main container is named as "Miniserver" and other containers that depend on it 
are named as "Peer0", "Peer1", and so on. The provided docker-compose file
inlcudes Miniserver and some more containers with names "PeerX" where X is a number. You may had more
containers to the docker-compose file by repeating the code block for one of the containers 
and change it's name appropriately.

The conatiner name for MiniServer is "Miniserver" and for the peers its "PeerX" where X is a number.

Open the Docker Toolbox Quickstart and use "cd" commands to traverse to your main folder

Execute the following:
```
docker-compose build
```
-f is not needed because I ensured only one instance of correct Dockerfile and docker-compose.yml
files are included in the folder.

Execute the following:
```
docker-compose up -d
```

After the above succeeds, run the following:
Miniserver is the container name.
```
docker exec -it Miniserver bash
```

use "cd" to traverse to project3
```
cd project3
```

Then run the MiniServer.java code,assuming Kademlia is the package name. 16 is the input parameter for the miniserver:
```
javac Kademlia/MiniServer.java
java Kademlia/MiniServer 16
```

You may get some notes/warnings during compilation that can be ignored.

Open another Toolbox Quickstart terminal, use "cd" commmands to traverse to your main folder
and directly run the following: Peer0 is the container name.
```
docker exec -it Peer0 bash
```

use "cd" to traverse to project3
```
cd project3
```

Then run the peer codes,assuming Kademlia is the package name. The input parameters for this is the "PeerX" where X becomes the node ID and 16 which is the max number of nodes for
the kademlia system in this project:
```
javac Kademlia/KademliaPeer.java
java Kademlia/KademliaPeer Peer0 16
```
You may get some notes/warnings during compilation that can be ignored.

Repeat the same for more Peer containers. Write appropriate names for the container name. 

Use the below commands to delete/clean up after you're done:
```
docker-compose down
```



