/*
 * DocClient.java
 */
import java.io.*;
import java.net.*;

/**
 * Client that sends the file to server
 *
 * @author Siddharth Bapat
 */

class DocClient {
    int port=1000;
    Socket soc;
    String test="()";
    InputStream ins;
    OutputStream outs;
    BufferedReader fins;

    /**
     * starts the client that reads the file and passes it to the server
     * @param file
     * @param targetserver
     */

    public void startclient(String file,String targetserver){
        int rec;
        String line;
        try{
            File f=new File(file);

            soc=new Socket(targetserver,port);
            byte[] data=test.getBytes();

            fins= new BufferedReader(new FileReader(f));
            ins=soc.getInputStream();
            outs=soc.getOutputStream();
            System.out.println("sending");
            while((line=fins.readLine())!=null){
                System.out.println(".");
                    byte[] filecontent=line.getBytes();
                    outs.write(filecontent);
            }
            System.out.println("sent file");
            outs.write(test.getBytes());
            System.out.println("The word count is:");
            while((rec=ins.read())>0){
                if((char)rec=='~'){
                    break;
                }
                System.out.print((char)rec);
            }
            System.out.println("");
        }
        catch(Exception e){
            System.out.println("Exception is :"+e);
        }
    }

    /**
     * Main function takes Filepath and server name as arguments
     * @param args
     */

    public static void main(String[] args) {
        new DocClient().startclient(args[0],args[1]);
    }
}
