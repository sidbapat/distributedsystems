*
 * DocServer.java
 *
 */

import java.io.*;
import java.net.*;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Server that counts the number of words
 *
 * @author Siddharth Bapat
 */

class DocServer {
    int port=1000;
    Socket inpsoc;
    ServerSocket serversoc;
    InputStream serins;
    OutputStream serouts;
    String response="Received ";
    int wordcount=0;

    /**
     * Starts the server
     */

    public void startserver(){
        int req;

            try {
                serversoc = new ServerSocket(port);
                while(true) {
                    inpsoc = serversoc.accept();
                    serins = inpsoc.getInputStream();
                    serouts = inpsoc.getOutputStream();
                    System.out.println("Received Information");
                    ArrayList<Character> check=new ArrayList<>();
                    check.add(' ');
                    while ((req=serins.read())>0) {

                        check.add((char)req);

                        if((char)req==',' || (char)req=='\n' || (char)req==' '){
                            wordcount++;
                        }
                        //server stops waiting for more input when it receives "()"
                        if((check.get(check.size()-1))==')' && (check.get(check.size()-2))=='('){
                            break;
                        }
                    }
                    System.out.println("");
                    System.out.println("Counting");


                    //send the response
                    System.out.println("Responded");
                    response=String.valueOf(wordcount);
                    wordcount=0;
                    byte[] res = response.getBytes();
                    serouts.write(res);
                    serouts.write("~".getBytes());

                }
            } catch (Exception e) {
                System.out.println("Server Exception is :"+e);
            }

    }

    /**
     * Main function
     * @param args
     */

    public static void main(String[] args) {
        new DocServer().startserver();
    }
}

